    
	// part 1.
	// config määrittelee pelipohjaa
	
	var config = {
		// jos selain tukee WebGL-piirtoa,
		// käytetään sitä, muuten käyttää
		// HTML5 canvas-piirtoa...
        type: Phaser.AUTO,
		// pelin fyysiset mitat px
        width: 1000,
        height: 800,
		
		// part 4.
		// pelin fysiikat
		physics: {
            default: 'arcade',
			// pelin painovoima
            arcade: {
                gravity: { y: 300 },
                debug: false
            }
        },
		// preload:
		// - kertoo mitä ladattavia asioita
		// peli sisältää
		// create:
		// - piirtää pelin taustakuvan, ym.
		// update:
		// - "peliloop", peli pyörii tässä osassa
		// kun se on käynnissä
		scene: {
            preload: preload,
            create: create,
            update: update
        }
    };
	
    var player;
    var stars;
    var platforms;
    var cursors;
    var score = 0;
	var gameOver = false;
    var scoreText;
	var music = '';
	var collect = '';
	var death = '';
	var kona = '';
	var kona2 = '';
	var jump = '';
	var hei = '';

	// game on "peliolio"
    var game = new Phaser.Game(config);

	// part 2.
    function preload ()
	// lataa ennen peliä muistiin pelin taustakuvan ja
	// pelihahmot
    {
		this.load.image('sky', 'assets/tausta44.png');
        this.load.image('ground', 'assets/platti.png');
        this.load.image('star', 'assets/stars.png');
        this.load.image('bomb', 'assets/poliisi.png');
        this.load.spritesheet('dude', 'assets/dusdeni.png', { frameWidth: 32, frameHeight: 48 });
		this.load.audio('music', 'assets/dolladolla.mp3');
		this.load.audio('starsound', 'assets/keräys.mp3');
		this.load.audio('death', 'assets/popo.mp3');
		this.load.audio('kona', 'assets/huh.mp3');
		this.load.audio('kona2', 'assets/dahell.mp3');
		this.load.audio('jump', 'assets/jump.mp3');
		this.load.audio('hei', 'assets/hei.mp3');
    }

    function create ()
    {
		// kuva sijoitetaan sen keskikohdan mukaan
		// this.add.image(400, 300, 'sky'
		// "parempi tapa
		this.add.image(0, 0, 'sky').setOrigin(0, 0,);
		
		platforms = this.physics.add.staticGroup();
		
		//maa
        platforms.create(2000, 1450, 'ground').setScale(4, 2).refreshBody();
		platforms.create(500, 1450, 'ground').setScale(4, 2).refreshBody();

		// muut platformit
        platforms.create(225, 1200, 'ground');
		platforms.create(1275, 1200, 'ground');
		platforms.create(1000, 500, 'ground');
		platforms.create(2200, 500, 'ground');
		platforms.create(2700, 675, 'ground');
		platforms.create(1600, 325, 'ground');
		platforms.create(2430, 1200, 'ground');
		platforms.create(1985, 1025, 'ground');
		platforms.create(790, 1025, 'ground');
		platforms.create(1400, 858, 'ground');
		platforms.create(300, 850, 'ground');
		platforms.create(600, 675, 'ground');
		platforms.create(1930, 675, 'ground');
		platforms.create(320, 325, 'ground');
		// pelihahmon fysiikat
        player = this.physics.add.sprite(1000, 1250, 'dude');

        player.setBounce(0.2);
        player.setCollideWorldBounds(true);
		// pelaajan animaatiot käveleminen oikealle ja vasemmalle ja kääntymiset
        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'turn',
            frames: [ { key: 'dude', frame: 4 } ],
            frameRate: 20
        });

        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
            frameRate: 10,
            repeat: -1
        });
		
        cursors = this.input.keyboard.createCursorKeys();
		// tähtien lisäys
        stars = this.physics.add.group({
            key: 'star',
            repeat: 100,
            setXY: { x: Phaser.Math.Between(0, 100) , y: Phaser.Math.Between(0, 100) , stepX: Phaser.Math.Between(0, 100)}
			
        });
		
        stars.children.iterate(function (child) {

            child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
			child.x = Phaser.Math.FloatBetween(0, 2400);
			child.y = Phaser.Math.FloatBetween(0, 1300);
		
        });
		
		// pommeja
		bombs = this.physics.add.group();
		// tässä scoreboard
        scoreText = this.add.text(16, 16, '$: 0', { fontSize: '32px', fill: 'white' });

        this.physics.add.collider(player, platforms);
        this.physics.add.collider(stars, platforms);
		this.physics.add.collider(bombs, platforms);

		// Tarkistaa, onko pelaaja päällekkäin jonkun tähden kanssa, jos hän kutsuu collectStar-toimintoa
        this.physics.add.overlap(player, stars, collectStar, null, this);
		
		this.physics.add.collider(player, bombs, hitBomb, null, this);
		kona = this.sound.add('kona', {loop : false, volume: 2});
		kona.play();
		//musiikki
		music = this.sound.add('music', {loop : true, volume: 0.3});
		music.play();
		
		this.physics.world.setBounds(0, 0, 2560, 1436);
		this.cameras.main.setBounds(0, 0, 2560, 1436);
		
		this.cameras.main.startFollow(player);
    }
	// pelaajan liikkeet
    function update ()
    {	
	
		scoreText.x = this.cameras.main.worldView.x;
		scoreText.y = this.cameras.main.worldView.y;
		
		
		if (gameOver)
		{
			return;
		}
	    if (cursors.left.isDown)
        {
            player.setVelocityX(-350);

            player.anims.play('left', true);
        }
        else if (cursors.right.isDown)
        {
            player.setVelocityX(350);

            player.anims.play('right', true);
        }
        else
        {
            player.setVelocityX(0);

            player.anims.play('turn');
        }

        if (cursors.up.isDown && player.body.touching.down)
        {
            player.setVelocityY(-360);
			jump = this.sound.add('jump', {loop : false, volume: 1.2});
			jump.play();
        }
	}
	// poistaa tähden näytöltä
	function collectStar (player, star)
    {
		// jättää kuitenkin tähden selaime
        star.disableBody(true, true);
		// pisteiden kasvatus, aina kun
		score += 10;
        scoreText.setText('$: ' + score);
		
		if (stars.countActive(true) === 0)
    {
        // Lisää kerättäviä tähtiä
        stars.children.iterate(function (child) {

            child.enableBody(true, child.x, child.y, true, true);

        });

        var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);
		
		var y = (player.y < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

        var bomb = bombs.create(x, 8, 'bomb');
        bomb.setBounce(1);
        bomb.setCollideWorldBounds(true);
        bomb.setVelocity(Phaser.Math.Between(-250, 300), 30);
		hei = this.sound.add('hei', {loop : false, volume: 1.3});
		hei.play();
		death = this.sound.add('death', {loop : true, volume: 0.3});
		death.play();
        bomb.allowGravity = false;
		
		var bomb = bombs.create(x, 6, 'bomb');
        bomb.setBounce(1);
        bomb.setCollideWorldBounds(true);
        bomb.setVelocity(Phaser.Math.Between(-300, 300), 30);
		death = this.sound.add('death', {loop : true, volume: 0.3});
		death.play();
        bomb.allowGravity = false;
		
		var bomb = bombs.create(x, 4, 'bomb');
        bomb.setBounce(1);
        bomb.setCollideWorldBounds(true);
        bomb.setVelocity(Phaser.Math.Between(-300, 300), 30);
		death = this.sound.add('death', {loop : true, volume: 0.3});
		death.play();
        bomb.allowGravity = false;

    }
	
	stareffect = this.sound.add('starsound', {loop : false});
	stareffect.play();
}

function hitBomb (player, bomb)
{	// kaikki fysiikat seis
    this.physics.pause();
	// pelaajan väri muuttuu
    player.setTint(0xff0000);
	// pelihahmo kääntyy pelaajaa kohti
    player.anims.play('turn');
	// update-toiminnon tulee tietää että peli on päällä
    gameOver = true;
	// extra, musiikki seis
	music.stop();
	kona2 = this.sound.add('kona2', {loop : false,});
	kona2.play();
}