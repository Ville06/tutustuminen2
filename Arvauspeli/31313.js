const luku = Math.floor(Math.random() * 10) + 1;
let arvaukset = 3;

function arvaa(arvaus) {
    arvaukset--;

    if (arvaus == luku) {
        viesti("Voitit!");
    } else if (arvaukset > 0) {
        viesti(`Väärin! ${arvaus < luku ? 'Luku on isompi.' : 'Luku on pienempi.'} Yrityksiä jäljellä: ${arvaukset}`);
    } else {
        viesti(`Hävisit! Luku oli ${luku}.`);
    }
}

function viesti(viesti) {
    document.getElementById("viesti").innerText = viesti;
}
