import random

def arvaa_numero():
    oikea_numero = random.randint(1, 10)
    arvaus_lkm = 0
    while arvaus_lkm < 3:
        arvaus_lkm += 1
        arvaus = int(input("Arvaa numero 1-10 väliltä: "))
        if arvaus < oikea_numero:
            print("Liian pieni!")
        elif arvaus > oikea_numero:
            print("Liian suuri!")
        else:
            print("Onneksi olkoon, arvasit oikein!")
            return
    print("Peli ohi, oikea numero oli", oikea_numero)

arvaa_numero()
