function kortin_arvot(nimi, email, tel, ika, radio) {
    let otsikko = document.getElementById("nimi");
    otsikko.innerHTML = nimi;
    let emailspan = document.getElementById("email");
    emailspan.innerHTML = email;
    let telspan = document.getElementById("tel");
    telspan.innerHTML = tel;
    let ikaspan = document.getElementById("ika");
    ikaspan.innerHTML = ika;
    let radiospan = document.getElementById("radio");
    radiospan.innerHTML = radio;
}

function parametrit() {
    const queryString = window.location.search;
    console.log(queryString);
    const urlParams = new URLSearchParams(queryString);
    const nimi = urlParams.get('nimi')
    console.log(nimi);
    const email = urlParams.get('email')
    console.log(email)
    const tel = urlParams.get('tel')
    console.log(tel)
    const ika = urlParams.get('ika')
    console.log(ika)
    const radio = urlParams.get('joku_radio')
    kortin_arvot(nimi, email, tel, ika, radio);
}

window.onload = parametrit;